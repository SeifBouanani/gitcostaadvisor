package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: StockHistorization
 *
 */
@Entity
@Table(name="t_stockHistorization")
public class StockHistorization implements Serializable {

	
	private Integer id;
	private Date date;
	private static final long serialVersionUID = 1L;

	public StockHistorization() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
   
}
