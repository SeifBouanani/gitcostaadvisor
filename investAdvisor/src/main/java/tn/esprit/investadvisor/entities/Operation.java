package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Operation
 *
 */
@Entity
@Table(name="t_operation")
public class Operation implements Serializable {

	
	private Integer id;
	private Integer quantity;
	private Date date;
	private Float transactionFree;
	private String type;
	private static final long serialVersionUID = 1L;

	public Operation() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}   
	public Float getTransactionFree() {
		return this.transactionFree;
	}

	public void setTransactionFree(Float transactionFree) {
		this.transactionFree = transactionFree;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
   
}
